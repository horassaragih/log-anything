const low = require('lowdb');
const fs = require('lowdb/adapters/FileSync');

const driver = new fs('log.json');
let db = low(driver)
let randomID = Math.floor(Math.random() * Math.floor(999))
exports.logthis = (content, name, timestamp = false) => {
    tmstmp = null;

    if(timestamp == true){
        let date = new Date;
        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();

        tmstp = day+'/'+month+'/'+year
    }

    db.get('log').push({id : randomID, name : name, content : content, timestamp : tmstmp}).write()
}