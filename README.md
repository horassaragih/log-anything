## log-anything
simplefy to make a record of something, such as message or other

### About
Okay, i make this module to easier the software engginer life to log any error, warning or other things in their program. In this module you will be able to record any activity of your program or other

### How it's works?
It will recieve the data from your program (make sure you configure your program to let it record the things you want) and it will save the data in the JSON file, it's so that easy. You can read the log.

### status
Still on development dude! if i done the version, i'll release it on npm

### support me or contribute in!
You can contribute in by fork this repo and add some feature, remove some bugs or other or you can give me some fund to help me to code and powering this project and the other project. <a href="http://paypal.me/explorerguy">![fund it](https://img.shields.io/badge/paypal-fund%20me!-blue)</a>

### license
It licensed on MIT License, which is this module is free on open. You can contribute in this things! If you want use it i hope you can put my name as the credit and as an accepriation for me. 

### The module i use
<ul>
  <li>LowDB (as JSON reader-writer)</li>
</ul>
